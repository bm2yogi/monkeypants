properties {
    $config='debug'
}

task Build {
    exec {
       msbuild ./monkeypants.sln /t:Build /p:"configuration=$config,RunOctoPack=true"
    }
}

task Clean {
    exec {
        msbuild ./monkeypants.sln /t:Clean /p:"configuration=$config,RunOctoPack=true"
    }
}

task Rebuild -depends Clean,Build

task default -depends Build